@extends ('layouts.master')

@section('content')

	
		<div class="blog-header">
	      <div class="container">
	        <h4 class="">Update {{ $user->name }}'s Profile</h4>
	      </div>
	    </div>

		<form method="POST" action="/users/{{ $user->id }}">

			{{ csrf_field() }}

			@include('users._form', $user)

			@include('layouts.error-form')


		</form>

		
	

@endsection