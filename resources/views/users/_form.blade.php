<div class="form-group">
				
	<label for="name">Name :</label>

	<input type="text" name="name" id="name" class="form-control" value="{{ $user->name or ''}}">

</div>

<div class="form-group">
	
	<label for="email">Email :</label>

	<input type="text" name="email" id="email" class="form-control" value="{{ $user->email or ''}}">

</div>

<div class="form-group">
	
	<label for="password">Password :</label>

	<input type="text" name="password" id="password" class="form-control">

</div>

<div class="form-group">
	
	<button type="submit" class="btn btn-primary">Submit</button>

</div>