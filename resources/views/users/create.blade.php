@extends ('layouts.master')

@section('content')

	
		<div class="blog-header">
	      <div class="container">
	        <h4 class="">Create New User</h4>
	      </div>
	    </div>

		<form method="POST" action="/users/add">

			{{ csrf_field() }}

			@include('users._form')

			@include('layouts.error-form')


		</form>

		
	

@endsection