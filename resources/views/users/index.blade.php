@extends ('layouts.master')

@section('content')

<div class="blog-header">
      <div class="container">
        <h1 class="blog-title">User List</h1>
        <p class="lead blog-description">list of available Users</p>
      </div>
    </div>

<table class="table">
  <thead class="thead-default">
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>

  	@if(!empty($users))
  		@foreach($users as $user)
			<tr>
			  <td>{{ $user->name }}</td>
			  <td>{{ $user->email }}</td>
			  <td>
			  @if(Auth::check())
			  	<a href="users/{{ $user->id }}">edit</a> | <a href="/users/{{$user->id}}/delete">delete</a></td>
			  @else
			  	-
			  @endif
			</tr>
    	@endforeach
    @endif

  </tbody>
</table>

@endsection