@extends ('layouts.master')

@section('content')

	
		<div class="blog-header">
	      <div class="container">
	        <h4 class="">Login</h4>
	      </div>
	    </div>

		<form method="POST" action="">

			{{ csrf_field() }}


			<div class="form-group">
				
				<label for="email">Email :</label>

				<input type="text" name="email" id="email" class="form-control">

			</div>

			<div class="form-group">
				
				<label for="password">Password :</label>

				<input type="password" name="password" id="password" class="form-control">

			</div>

			<div class="form-group">
				
				<button type="submit" class="btn btn-primary">Submit</button>

			</div>

			@include('layouts.error-form')


		</form>

		
	

@endsection