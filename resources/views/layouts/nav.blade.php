
    <div class="blog-masthead">
      <div class="container">
        <nav class="nav blog-nav">
          <a class="nav-link active" href="/">Home</a>
          @if(Auth::check())
          	
          <a class="nav-link" href="/users/add">New User</a>
          	<a class="nav-link ml-auto" href="/logout">Sign out</a>
          	@else
          	<a class="nav-link ml-auto" href="/login">Sign in</a>
      		@endif
        </nav>
      </div>
    </div>