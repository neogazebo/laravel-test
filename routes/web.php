<?php

Route::get('/users', 'UsersController@index')->name('home');

Route::get('/users/add', 'UsersController@create')->name('user/add');

Route::post('/users/add', 'UsersController@store');

Route::get('/users/{users}/delete', 'UsersController@destroy');

Route::get('/users/{user}', 'UsersController@edit')->name('user/edit');;

Route::post('/users/{user}', 'UsersController@update');

Route::get('/login', 'SessionController@create')->name('login');

Route::post('/login', 'SessionController@store');

Route::get('/logout', 'SessionController@destroy');

Route::get('/', function () {
    return redirect('/users');
});